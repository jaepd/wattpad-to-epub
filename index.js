"use strict";

let fs = require("fs");
let nfetch = require("node-fetch");
let Peepub = require("pe-epub");

function getBookFilePath (id) {
	return `./bin/${id}.epub`;
}

function genEpub(json){
	console.log(`generating EPUB for "${json.title}"...`);
	const newEpub = new Peepub(json);
	const filePath = getBookFilePath(json.id);

	return newEpub.create(filePath)
	.then((fp) => {
		console.log(fp);
	})
	.catch((reason) => {
		console.error(reason);
	});
}

function fetchStoryPart (p) {
	const url = `http://mobile.wattpad.com/apiv2/storytext?id=${p.id}&include_paragraph_id=0`;
	return nfetch(url)
	.catch(err => {
		console.log(`Couldn't download ${p.id}: ${err}`);
	})
	.then(res => res.text())
};

function fetchStoryInfo (id) {
	const url = `http://www.wattpad.com/api/v3/stories/${id}?drafts=0&include_deleted=1`;
	console.log(`Fetching story #${id} metadata...`);
	return nfetch(url)
	.then(res => res.json())
	.catch(err => console.log(`Couldn't download ${id}: ${err}`));
};

function shouldBeUpdated (story) {
	console.log(`Checking if "${story.title}" should be updated...`);
	try {
		const stats = fs.lstatSync(getBookFilePath(story.id));

		if(Date.parse(stats.mtime) < Date.parse(story.modifyDate)){
			//the book was changed and needs to be remade
			console.log(`X - OUTDATED: ${story.title}`);
		} else {
			//no need to continue
			console.log(`@ - UPTODATE: ${story.title}`);
			return false;
		}
	} catch(e) {
		//doesn't exist, throws an error saying it can't find it
		console.log(`0 - NOT FOUND: ${story.title}`);
	}

	return true;
}

function processStoryInfo (info) {
	console.log(`Processing metadata for "${info.title}"...`);
	const story = {
		id: info.id,
		title: info.title,
		cover: info.cover,
		lastModified: info.modifyDate,
		creator: {
			name: info.user.name,
			role:"aut"
		},
		pages: info.parts.map(p => {
			return {
				id: p.id,
				title: p.title,
				toc: true
			};
		}),
		subjects: [
			"wattpad"
		]
	};

	return story;
};

function generateBooks (ids) {
	return Promise.all(ids.map(id => fetchStoryInfo(id)))
	.then(infos => infos
		.map(info => processStoryInfo(info))
		.filter(story => shouldBeUpdated(story))
	)
	.then(stories => Promise.all(stories.map(story => {
		console.log(`Downloading ${story.title} parts...`);

		return Promise.all(story.pages.map(p => fetchStoryPart(p)))
		.then(parts => {
			parts.forEach((p, i) => story.pages[i].body = p);
			return story;
		})
	})))
	.then(stories => Promise.all(stories.map(story => genEpub(story))))
	;
};

function parseMyList(jsonFile){
	// update: the v4 api works now!!

	// test
	/*
	microservices book by wattpad eng team
	const testId = '64615316';
	generateBooks([testId]);
	*/

	const mylist = JSON.parse(fs.readFileSync(jsonFile));
	const ids = mylist.stories
	.map(s => s.id);

	generateBooks(ids);
}

parseMyList("./mylist.json");
