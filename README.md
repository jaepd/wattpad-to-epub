# wattpad-parser
Generate ebooks (`.epub`) via a variety of wattpad APIs and the `pe-epub` node package. Add the generate ebook to your Calibre library for perfectly functional ebook versions of Wattpad stories of any format.

## Requirements
- Node.js

## How to Use
0. Make a `bin/` directory in the project's home dir.
1. Install deps:
```
npm install
```
5. You'll add your story's id (and I also recommend adding a name just to keep track of stuff in the JSON) to a new element to the `story` list in `mylist.json`. The `mylist.json` that is found in the repository is just an example of the necessary structure - you'll get errors if you don't delete/replace the dummy data in there.
6. For each story you want to add [example: "Journey to the Center of Microservices" by the Wattpad devs]:
	1. Go to any page in the story.
	2. That huge number in the last section of the URL is the story id [in the example with the URL being `https://www.wattpad.com/227192879-journey-to-the-center-of-microservices-orion` the id is `227192879`] and has to be correct. Add a new element to the `story` array in `mylist.json` with this new id.
	3. The `title` field in `mylist.json` is never used! It's just the best way to track which IDs are which stories.
7. Run the script with `npm start` on a working internet connection and watch your books being created! The script will log the progress.
8. When the script is done (there'll be a message about it finishing), find all your ebooks in `bin/`.

## Notes
- The generated ebooks have the Wattpad story's `id` as the name (i.e. `227192879.epub`). However, the ebook metadata has correctly stored the name of the story.
- There may be folders in the `bin/` folder - these are just artifacts of the `pe-epub` process.
- Have any of your stories been updated online since you generated them? This happens quite often, since Wattpad encourages serial writing. This package also supports updating. Run the script, and it will re-generate if it finds a story id without a corresponding .epub in `bin/`, *or* if the ebook file in `bin/` was last modified before the online story's last modification.
